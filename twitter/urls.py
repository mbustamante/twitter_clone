from django.conf.urls import patterns, include, url
from django.contrib import admin
admin.autodiscover()


urlpatterns = patterns('',
	# url(r'^$', 'twitter.views.welcome', name='welcome'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^register/$', 'twitter.views.register_public', name='register_public'),
    url(r'^register_private/$', 'twitter.views.register_private', name='register_private'),
    url(r'^login/$', 'twitter.views.user_login', name='login'),
    url(r'^restricted/', 'twitter.views.restricted', name='restricted'),
    url(r'^logout/', 'twitter.views.user_logout', name='logout'),
    url(r'^home/?$', "twitter.views.user_home", name="user_home"),
    url(r'^create_tweet/?$', "twitter.views.create_tweet", name="create_tweet"),
    url(r'^hashtag/?$', "twitter.views.hashtag", name="hashtag"),
    url(r'^trendings/?$', "twitter.views.trendings", name="trendings"),
    url(r'^user_mention/?$', "twitter.views.user_mention", name="user_mention"),
    url(r'^geo_trendings/?$', "twitter.views.geo_trendings", name="geo_trendings"),
)