var date_start = "";
var date_end = "";
var latitude = "";
var longitude = "";

//csrftoken
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}


// HIGHCHART

 


$(document).ready(function () {
    var currentDate = new Date();
    $('#datepickerStart, #datepickerEnd').datepicker({
        format: "yyyy/dd/mm",
        todayBtn: "linked",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
  
    $('#datepickerStart, #datepickerEnd').datepicker("setDate", currentDate);
    date_start = $("#datepickerStart").val();
    date_end = $("#datepickerEnd").val();
            
});

//Nuevo grupo
$(document).ready(function() {

          
        getLocation();

          $("#submitTweet").click(function() {
              var tweet = $('#inputTweet').val();
              
              // console.log("d"+latitude);
              if (tweet!="")
              {
                  $.post("/create_tweet/",
                    {
                      new_tweet : tweet,
                      latitude: latitude,
                      longitude: longitude,
                      csrfmiddlewaretoken: getCookie('csrftoken')
                    },
                    function(data){
                      console.log(data);
                      if (data["server_response"] ==1)
                      {
                        $("#table_comments").append('<tr><td>'+data["tweet"].text+'</td><td>'+data["tweet"].date+'</td><tr>');
                      }
                    },
                    "json"
                  );
              }
              
          });


          $("#Hashtag").click(function() {
              var hashtag = $('#inputHashtag').val();
              if (hashtag!="")
              {
                  $.post("/hashtag/",
                    {
                      new_hashtag : hashtag,
                      csrfmiddlewaretoken: getCookie('csrftoken')
                    },
                    function(data){
                      $("#table_comments tr").remove();
                      $("#table_comments").append('<tr><th>Contenido</th>><th>Fecha</th></tr>');
                      console.log(data);
                      $.each(data['tweets'], function(i, item) {
                              $("#table_comments").append('<tr><td>'
                                +item.text+'</td><td>'+item.date+'</td><tr>');
                       
                                    //alert(item);
                                    //console.log(item.fields.contenido);
                      });
                    },
                    "json"
                  );
              }
              
          });

        $("#Trendings").click(function() {

                  $.post("/trendings/",
                    {
                      csrfmiddlewaretoken: getCookie('csrftoken')
                    },
                    function(data){
                      $("#table_comments tr").remove();
                      $("#table_comments").append('<tr><th>Palabra</th>><th>Cantidad</th></tr>');
                      console.log(data);
                      $.each(data['trendings'], function(i, item) {
                              $("#table_comments").append('<tr><td>'
                                +item.word+'</td><td>'+item.cont+'</td><tr>');
                       
                                    //alert(item);
                                    //console.log(item.fields.contenido);
                      });
                    },
                    "json"
                  );
              
              
          });

        $("#UserMention").click(function() {
              var user = $('#inputUserMention').val();
              if (user!="")
              {
                  $.post("/user_mention/",
                    {
                      new_user : user,
                      csrfmiddlewaretoken: getCookie('csrftoken')
                    },
                    function(data){
                      $("#table_comments tr").remove();
                      $("#table_comments").append('<tr><th>Palabra</th>><th>Cantidad</th></tr>');
                      console.log(data);
                      $.each(data['trendings'], function(i, item) {
                              $("#table_comments").append('<tr><td>'
                                +item.word+'</td><td>'+item.cont+'</td><tr>');
                       
                                    //alert(item);
                                    //console.log(item.fields.contenido);
                      });
                    },
                    "json"
                  );
              }
              
          });

          $("#GeoTrending").click(function() {
              var location = $('#inputGeoTrending').val();
              if (location!="")
              {
                  $.post("/geo_trendings/",
                    {
                      new_location : location,
                      csrfmiddlewaretoken: getCookie('csrftoken')
                    },
                    function(data){
                      $("#table_comments tr").remove();
                      $("#table_comments").append('<tr><th>Hashtag</th>><th>cantidad</th></tr>');
                      console.log(data);
                      $.each(data['trendings'], function(i, item) {
                              $("#table_comments").append('<tr><td>'
                                +item.word+'</td><td>'+item.cont+'</td><tr>');
                       
                                    //alert(item);
                                    //console.log(item.fields.contenido);
                      });
                    },
                    "json"
                  );
              }
              
          });
          

          
});

function getLocation() {
    if (navigator.geolocation) {
        a = navigator.geolocation.getCurrentPosition(showPosition);
    }
}

function showPosition(position) {
    latitude = position.coords.latitude;
    longitude =  position.coords.longitude;
    // console.log(latitude)
}


$(document).on('click' , "#listGroups .dropdown-menu .confPagesByGroup" , function(){
          var current_group = $(this).attr('value');   
          $('#submitPage').attr('value',current_group);
          console.log(current_group);

          for( i=0 ; i< session_groups_pages[current_group].length ; i++)
          {
              console.log(i);
              $('#tablePagesByGroup').append('<tr><td>'+session_groups_pages[current_group][i]+'</td><td><span class="glyphicon glyphicon-trash"></span></td></tr>');
          }
          $('.glyphicon-trash').tooltip({ title: 'eliminar' , placement: 'auto top'})
});