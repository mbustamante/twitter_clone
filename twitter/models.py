from django.db import models
from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser
from djangotoolbox.fields import ListField, EmbeddedModelField
from django_mongodb_engine.contrib import MongoDBManager
# from django.contrib.auth.models import User


# class UserProfile(models.Model):
#     user = EmbeddedModelField('User')
#     account_type = models.BooleanField(default=True)    #0: privado, 1:publico
#     location = models.CharField(max_length=20)

#     # def __unicode__(self):
#     #     return '%s' % (self.user.username)

 
class TwitterUserManager(BaseUserManager):
    def create_user(self, email, username, first_name, last_name, account_type, location, password):
        if not email:
            raise ValueError('Users must have a password')
        if not email:
            raise ValueError('Users must have an email address')
        if not email:
            raise ValueError('Users must have an email username')
        if not first_name:
            raise ValueError('Users must have a first name')
        if not last_name:
            raise ValueError('Users must have a last name')
        if not account_type:
            raise ValueError('Users must have an account type')
        if not location:
            raise ValueError('Users must have a location')
 
        user = self.model(
            email=TwitterUserManager.normalize_email(email),
            username = username,
            first_name = first_name,
            last_name = last_name,
            account_type = account_type,
            location = location,
        )
 
        user.set_password(password)
        user.save(using=self._db)
        return user
 
    def create_superuser(self, email, username, first_name, last_name, account_type, location, password):
        user = self.model(
            email=TwitterUserManager.normalize_email(email),
            username = username,
            first_name = first_name,
            last_name = last_name,
            account_type = account_type,
            location = location,
        )
        user.set_password(password)
        user.is_admin = True
        user.save(using=self._db)
        return user
 
 
class TwitterUser(AbstractBaseUser):
    username = models.CharField(max_length=20, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=254, unique=True, db_index=True)
    account_type = models.CharField(max_length = 50, default='public')
    location = models.CharField(max_length= 20)
 
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
 
    objects = TwitterUserManager()
 
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name','last_name','account_type','location']
 
    def get_full_name(self):
        # For this case we return email. Could also be User.first_name User.last_name if you have these fields
        return self.first_name
 
    def get_short_name(self):
        # For this case we return email. Could also be User.first_name if you have this field
        return self.first_name
 
    def __unicode__(self):
        return self.email
 
    def has_perm(self, perm, obj=None):
        # Handle whether the user has a specific permission?"
        return True
 
    def has_module_perms(self, app_label):
        # Handle whether the user has permissions to view the app `app_label`?"
        return True
 
    @property
    def is_staff(self):
        # Handle whether the user is a member of staff?"
        return self.is_admin

    # def __unicode__(self):
    #     return '%s' % (self.username)


class Point(models.Model):
    latitude = models.FloatField()
    longtitude = models.FloatField()

class Zone(models.Model):
    loc = EmbeddedModelField(Point)
    name = models.CharField(max_length=30)
    objects = MongoDBManager()

    class MongoMeta:
        indexes =[
            {'fields': [('loc', '2d')], 'min': -42, 'max': 80},
        ]





class Tweet(models.Model):
    created_on = models.DateTimeField(auto_now_add=True, null=True)
    text = models.CharField(max_length=140)
    user_owner = models.CharField(max_length=20)
    user_mentions = ListField()
    hashtags = ListField()
    location = EmbeddedModelField(Point)
    zone = models.CharField(max_length=30)


    objects = MongoDBManager()

    class MongoMeta:
        indexes =[
            {'fields': [('location', '2d')], 'min': -42, 'max': 80},
        ]
    # def __unicode__(self):
    #     return '%s' % (self.text)




class MapReduceHashtag(models.Model):
    value = models.TextField()

    objects = MongoDBManager()

class MapReduceTrendings(models.Model):
    value = models.TextField()

    objects = MongoDBManager()


class MapReduceUserMention(models.Model):
    value = models.TextField()

    objects = MongoDBManager()

class MapReduceGeoTrending(models.Model):
    value = models.TextField()

    objects = MongoDBManager()



# class Location:
#     point = GeoPointField()
#     locate = models.CharField(max_length=50)
