from models import TwitterUser
from django import forms

class TwitterUserPublicForm(forms.ModelForm):
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    class Meta:
        model = TwitterUser
        fields = ('email', 'username', 'first_name','last_name','location')


    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(TwitterUserPublicForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user

class TwitterUserPrivateForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = TwitterUser
        fields = ('email', 'username', 'first_name','last_name','account_type','location')

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(TwitterUserPrivateForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user