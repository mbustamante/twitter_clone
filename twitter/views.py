# -*- coding: utf-8 -*-

from forms import TwitterUserPrivateForm, TwitterUserPublicForm
from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.shortcuts import render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth import get_user_model, authenticate, login, logout
from django.views.decorators.csrf import ensure_csrf_cookie
from models import Tweet, Point, Zone, MapReduceHashtag, MapReduceTrendings, MapReduceUserMention
import json
import time

User = get_user_model()
# def welcome(request):
#     return render_to_response('twitter/welcome.html')

def register_public(request):

    context = RequestContext(request)
    registered = False

    if request.method == 'POST':
        print request.POST

        user_form = TwitterUserPublicForm(data=request.POST)
        print user_form
        if user_form.is_valid():
            user = user_form.save()
            registered = True
        else:
            print user_form.errors

    else:
        user_form = TwitterUserPublicForm()
    return render_to_response(
            'twitter/register.html',
            {'user_form': user_form,  'registered': registered},
            context)

def register_private(request):

    context = RequestContext(request)
    registered = False

    if request.method == 'POST':

        user_form = TwitterUserPrivateForm(data=request.POST)

        if user_form.is_valid():
            user = user_form.save()
            registered = True
        else:
            print user_form.errors

    else:
        user_form = TwitterUserPrivateForm()
    return render_to_response(
            'twitter/register.html',
            {'user_form': user_form,  'registered': registered},
            context)



def user_login(request):

    context = RequestContext(request)

    if request.method == 'POST':

        username = request.POST['username']
        password = request.POST['password']

        user = authenticate(username=username, password=password)

        if user:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/home')
            else:
                return HttpResponse("Your Rango account is disabled.")
        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")
    else:
        return render_to_response('twitter/login.html', {}, context)


@login_required
def restricted(request):
    return HttpResponse("Since you're logged in, you can see this text!")


@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect('/login')




@login_required
@ensure_csrf_cookie
def user_home(request):
    # print request
    name = request.user.username
    tweets = Tweet.objects.filter(user_owner = name).order_by("-created_on")
    print tweets
    return render_to_response('twitter/home.html', {'name':name,'tweets':tweets},context_instance=RequestContext(request))


@login_required
def create_tweet(request):
    # print request.POST
    if request.POST.has_key('new_tweet'):
        print "NUEVO TWEET"
        try :
            raw = request.POST['new_tweet']
            # print request.POST['latitude']
            # print request.POST['longitude']
            if request.POST['latitude']!="": 
                lat = float(request.POST['latitude'])
                lon = float(request.POST['longitude'])
            else:
                lat = -16.394515
                lon = -71.533423
            raw_tokens = raw.split()
            hashtags = []
            mentions = []
            for token in raw_tokens:
                if token[0] == '#':
                    hashtags.append(token[1:])
                if token[0] == '@':
                    mentions.append(token[1:])
            # print request.user.username
            zone = Zone.objects.raw_query({'loc' :{'$near' : {'latitude':lat, 'longtitude':lon}}})[0]
            Tweet(text=raw,hashtags=hashtags,user_mentions=mentions, user_owner=request.user.username,
                location=Point(latitude=lat,longtitude=lon),zone = zone.name).save()
            # print "INSERTADO"
            is_add = 1
        except Exception as E:
            print E
            is_add = 0
        response_dict = {} 
        # print message                                     
        response_dict['server_response'] = is_add
        ## print response_dict
        # print "el grupo es ", id_group
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
    else:
        return render_to_response('twitter/home.html', context_instance=RequestContext(request))

@login_required
@ensure_csrf_cookie
def hashtag(request):

    if request.POST.has_key('new_hashtag'):
        hashtag = request.POST['new_hashtag']
        mapfunc = """
        function() {
            for (var i=0; i < this.hashtags.length; i++) {
                    emit(this.hashtags[i], {text:this.text,date:this.created_on});
            }
        }
        """

        reducefunc = """
        function reduce(key, values) {
            return {hashtag:key, tweets:values};
        }
        """
        print "LLEGO"
        name = request.user.username
        try:
            Tweet.objects.map_reduce(mapfunc, reducefunc, 'twitter_mapreducehashtag')
            result = MapReduceHashtag.objects.raw_query({"_id":hashtag})
            tweets =[]
            if result[0].value.has_key('tweets'):
                result = result[0].value["tweets"]
                result = sorted(result, key=lambda k: k['date'] ,reverse=True)[:50]
                for r in result:
                    r["date"] = r["date"].strftime("%Y-%m-%d %H:%M:%S")
                    tweets.append(r)
            else:
                # print result[0].value
                r = result[0].value
                r["date"] = r["date"].strftime("%Y-%m-%d %H:%M:%S")
                tweets.append(r)
            response_dict = {"tweets":tweets}
        except Exception as e:
            print e
            response_dict ={}
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
        # return render_to_response('twitter/home.html', {'name':name,'tweets':tweets},context_instance=RequestContext(request))
    else:
        return render_to_response('twitter/home.html', context_instance=RequestContext(request))



@login_required
@ensure_csrf_cookie
def trendings(request):

    if request.POST:
        # mapfunc = "function() {stop_words = 'algún,alguna,algunas,alguno,algunos,ambos,ampleamos,ante,antes,aquel,aquellas,aquellos,aqui,arriba,atras,bajo,bastante,bien,cada,cierta,ciertas,cierto,ciertos,como,con,conseguimos,conseguir,consigo,consigue,consiguen,consigues,cual,cuando,dentro,desde,donde,dos,el,ellas,ellos,empleais,emplean,emplear,empleas,empleo,en,encima,entonces,entre,era,eramos,eran,eras,eres,es,esta,estaba,estado,estais,estamos,estan,estoy,fin,fue,fueron,fui,fuimos,gueno,ha,hace,haceis,hacemos,hacen,hacer,haces,hago,incluso,intenta,intentais,intentamos,intentan,intentar,intentas,intento,ir,la,largo,las,lo,los,mientras,mio,modo,muchos,muy,nos,nosotros,otro,para,pero,podeis,podemos,poder,podria,podriais,podriamos,podrian,podrias,por,por qué,porque,primero,puede,pueden,puedo,quien,sabe,sabeis,sabemos,saben,saber,sabes,ser,si,siendo,sin,sobre,sois,solamente,solo,somos,soy,su,sus,también,teneis,tenemos,tener,tengo,tiempo,tiene,tienen,todo,trabaja,trabajais,trabajamos,trabajan,trabajar,trabajas,trabajo,tras,tuyo,ultimo,un,una,unas,uno,unos,usa,usais,usamos,usan,usar,usas,uso,va,vais,valor,vamos,van,vaya,verdad,verdadera,verdadero,vosotras,vosotros,voy,yo,él,ésta,éstas,éste,éstos,última,últimas,último,últimos,a,añadió,aún,actualmente,adelante,además,afirmó,agregó,ahí,ahora,al,algún,algo,alguna,algunas,alguno,algunos,alrededor,ambos,ante,anterior,antes,apenas,aproximadamente,aquí,así,aseguró,aunque,ayer,bajo,bien,buen,buena,buenas,bueno,buenos,cómo,cada,casi,cerca,cierto,cinco,comentó,como,con,conocer,consideró,considera,contra,cosas,creo,cual,cuales,cualquier,cuando,cuanto,cuatro,cuenta,da,dado,dan,dar,de,debe,deben,debido,decir,dejó,del,demás,dentro,desde,después,dice,dicen,dicho,dieron,diferente,diferentes,dijeron,dijo,dio,donde,dos,durante,e,ejemplo,el,ella,ellas,ello,ellos,embargo,en,encuentra,entonces,entre,era,eran,es,esa,esas,ese,eso,esos,está,están,esta,estaba,estaban,estamos,estar,estará,estas,este,esto,estos,estoy,estuvo,ex,existe,existen,explicó,expresó,fin,fue,fuera,fueron,gran,grandes,ha,había,habían,haber,habrá,hace,hacen,hacer,hacerlo,hacia,haciendo,han,hasta,hay,haya,he,hecho,hemos,hicieron,hizo,hoy,hubo,igual,incluso,indicó,informó,junto,la,lado,las,le,les,llegó,lleva,llevar,lo,los,luego,lugar,más,manera,manifestó,mayor,me,mediante,mejor,mencionó,menos,mi,mientras,misma,mismas,mismo,mismos,momento,mucha,muchas,mucho,muchos,muy,nada,nadie,ni,ningún,ninguna,ningunas,ninguno,ningunos,no,nos,nosotras,nosotros,nuestra,nuestras,nuestro,nuestros,nueva,nuevas,nuevo,nuevos,nunca,o,ocho,otra,otras,otro,otros,para,parece,parte,partir,pasada,pasado,pero,pesar,poca,pocas,poco,pocos,podemos,podrá,podrán,podría,podrían,poner,por,porque,posible,próximo,próximos,primer,primera,primero,primeros,principalmente,propia,propias,propio,propios,pudo,pueda,puede,pueden,pues,qué,que,quedó,queremos,quién,quien,quienes,quiere,realizó,realizado,realizar,respecto,sí,sólo,se,señaló,sea,sean,según,segunda,segundo,seis,ser,será,serán,sería,si,sido,siempre,siendo,siete,sigue,siguiente,sin,sino,sobre,sola,solamente,solas,solo,solos,son,su,sus,tal,también,tampoco,tan,tanto,tenía,tendrá,tendrán,tenemos,tener,tenga,tengo,tenido,tercera,tiene,tienen,toda,todas,todavía,todo,todos,total,tras,trata,través,tres,tuvo,un,una,unas,uno,unos,usted,va,vamos,van,varias,varios,veces,ver,vez,y,ya,yo'stop_words = stop_words.split(','); clean_text = this.text.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g,''); tokens = clean_text.split(' ');for(var i = 0 ; i< tokens.length ; i++){if(stop_words.indexOf(tokens[i])==-1){emit( tokens[i],1 );}}}"      
        mapfunc = """
            function() {
                stop_words = ["que","no","El","La","del","de","a","","y","o","se","RT","alguna","algunas","alguno","algunos","ambos","ampleamos","ante","antes","aquel","aquellas","aquellos","aqui","arriba","atras","bajo","bastante","bien","cada","cierta","ciertas","cierto","ciertos","como","con","conseguimos","conseguir","consigo","consigue","consiguen","consigues","cual","cuando","dentro","desde","donde","dos","el","ellas","ellos","empleais","emplean","emplear","empleas","empleo","en","encima","entonces","entre","era","eramos","eran","eras","eres","es","esta","estaba","estado","estais","estamos","estan","estoy","fin","fue","fueron","fui","fuimos","gueno","ha","hace","haceis","hacemos","hacen","hacer","haces","hago","incluso","intenta","intentais","intentamos","intentan","intentar","intentas","intento","ir","la","largo","las","lo","los","mientras","mio","modo","muchos","muy","nos","nosotros","otro","para","pero","podeis","podemos","poder","podria","podriais","podriamos","podrian","podrias","por","porque","primero","puede","pueden","puedo","quien","sabe","sabeis","sabemos","saben","saber","sabes","ser","si","siendo","sin","sobre","sois","solamente","solo","somos","soy","su","sus","teneis","tenemos","tener","tengo","tiempo","tiene","tienen","todo","trabaja","trabajais","trabajamos","trabajan","trabajar","trabajas","trabajo","tras","tuyo","ultimo","un","una","unas","uno","unos","usa","usais","usamos","usan","usar","usas","uso","va","vais","valor","vamos","van","vaya","verdad","verdadera","verdadero","vosotras","vosotros","voy","yo"]
                clean_text = this.text.replace(/[\.',-\/#!$%\^&\*;:{}=\-_`~()]/g,'');
                tokens = clean_text.split(' ');
                for(var i = 0 ; i< tokens.length ; i++)
                {
                    if(stop_words.indexOf(tokens[i])==-1)
                    {
                        emit( tokens[i],1 );
                    }
                }
            }
        """
        mapfunc = mapfunc.decode("utf8")

        reducefunc = """
            function reduce(key, values) {
              return values.length;
            }
        """
        print "LLEGO"
        name = request.user.username

        try:
            Tweet.objects.map_reduce(mapfunc, reducefunc, 'twitter_mapreducetrendings')
            result = MapReduceTrendings.objects.all().order_by('-value')[:50]
            trendings = []
            for trending in result:
                word = trending.id.replace(u"‘","").replace(u"“","").replace(u"…","")
                t = {"word":word, "cont":trending.value}
                trendings.append(t)
            response_dict = {"trendings":trendings}
            
        except Exception as e:
            print e
            response_dict = {} 
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
        # return render_to_response('twitter/home.html', {'name':name,'tweets':tweets},context_instance=RequestContext(request))
    else:
        return render_to_response('twitter/home.html', context_instance=RequestContext(request))


@login_required
@ensure_csrf_cookie
def user_mention(request):

    if request.POST.has_key('new_user'):
        user = request.POST['new_user']
        mapfunc = """
        function() {
            for (var i=0; i < this.user_mentions.length; i++) {
                    emit(this.user_mentions[i], {text:this.text,date:this.created_on});
            }
        }
        """

        reducefunc = """
        function reduce(key, values) {
            return {user:key, tweets:values};
        }
        """
        print "LLEGO"
        name = request.user.username
        try:
            Tweet.objects.map_reduce(mapfunc, reducefunc, 'twitter_mapreduceusermention')
            result = MapReduceUserMention.objects.raw_query({"_id":user})
            tweets =[]
            if result[0].value.has_key('tweets'):
                result = result[0].value["tweets"]
                result = sorted(result, key=lambda k: k['date'] ,reverse=True)[:50]
                for r in result:
                    r["date"] = r["date"].strftime("%Y-%m-%d %H:%M:%S")
                    tweets.append(r)
            else:
                r = result[0].value
                r["date"] = r["date"].strftime("%Y-%m-%d %H:%M:%S")
                tweets.append(r)
            response_dict = {"tweets":tweets}
        except Exception as e:
            print e
            response_dict ={}
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
        # return render_to_response('twitter/home.html', {'name':name,'tweets':tweets},context_instance=RequestContext(request))
    else:
        return render_to_response('twitter/home.html', context_instance=RequestContext(request))


@login_required
@ensure_csrf_cookie
def geo_trendings(request):

    if request.POST.has_key("new_location"):
        # mapfunc = "function() {stop_words = 'algún,alguna,algunas,alguno,algunos,ambos,ampleamos,ante,antes,aquel,aquellas,aquellos,aqui,arriba,atras,bajo,bastante,bien,cada,cierta,ciertas,cierto,ciertos,como,con,conseguimos,conseguir,consigo,consigue,consiguen,consigues,cual,cuando,dentro,desde,donde,dos,el,ellas,ellos,empleais,emplean,emplear,empleas,empleo,en,encima,entonces,entre,era,eramos,eran,eras,eres,es,esta,estaba,estado,estais,estamos,estan,estoy,fin,fue,fueron,fui,fuimos,gueno,ha,hace,haceis,hacemos,hacen,hacer,haces,hago,incluso,intenta,intentais,intentamos,intentan,intentar,intentas,intento,ir,la,largo,las,lo,los,mientras,mio,modo,muchos,muy,nos,nosotros,otro,para,pero,podeis,podemos,poder,podria,podriais,podriamos,podrian,podrias,por,por qué,porque,primero,puede,pueden,puedo,quien,sabe,sabeis,sabemos,saben,saber,sabes,ser,si,siendo,sin,sobre,sois,solamente,solo,somos,soy,su,sus,también,teneis,tenemos,tener,tengo,tiempo,tiene,tienen,todo,trabaja,trabajais,trabajamos,trabajan,trabajar,trabajas,trabajo,tras,tuyo,ultimo,un,una,unas,uno,unos,usa,usais,usamos,usan,usar,usas,uso,va,vais,valor,vamos,van,vaya,verdad,verdadera,verdadero,vosotras,vosotros,voy,yo,él,ésta,éstas,éste,éstos,última,últimas,último,últimos,a,añadió,aún,actualmente,adelante,además,afirmó,agregó,ahí,ahora,al,algún,algo,alguna,algunas,alguno,algunos,alrededor,ambos,ante,anterior,antes,apenas,aproximadamente,aquí,así,aseguró,aunque,ayer,bajo,bien,buen,buena,buenas,bueno,buenos,cómo,cada,casi,cerca,cierto,cinco,comentó,como,con,conocer,consideró,considera,contra,cosas,creo,cual,cuales,cualquier,cuando,cuanto,cuatro,cuenta,da,dado,dan,dar,de,debe,deben,debido,decir,dejó,del,demás,dentro,desde,después,dice,dicen,dicho,dieron,diferente,diferentes,dijeron,dijo,dio,donde,dos,durante,e,ejemplo,el,ella,ellas,ello,ellos,embargo,en,encuentra,entonces,entre,era,eran,es,esa,esas,ese,eso,esos,está,están,esta,estaba,estaban,estamos,estar,estará,estas,este,esto,estos,estoy,estuvo,ex,existe,existen,explicó,expresó,fin,fue,fuera,fueron,gran,grandes,ha,había,habían,haber,habrá,hace,hacen,hacer,hacerlo,hacia,haciendo,han,hasta,hay,haya,he,hecho,hemos,hicieron,hizo,hoy,hubo,igual,incluso,indicó,informó,junto,la,lado,las,le,les,llegó,lleva,llevar,lo,los,luego,lugar,más,manera,manifestó,mayor,me,mediante,mejor,mencionó,menos,mi,mientras,misma,mismas,mismo,mismos,momento,mucha,muchas,mucho,muchos,muy,nada,nadie,ni,ningún,ninguna,ningunas,ninguno,ningunos,no,nos,nosotras,nosotros,nuestra,nuestras,nuestro,nuestros,nueva,nuevas,nuevo,nuevos,nunca,o,ocho,otra,otras,otro,otros,para,parece,parte,partir,pasada,pasado,pero,pesar,poca,pocas,poco,pocos,podemos,podrá,podrán,podría,podrían,poner,por,porque,posible,próximo,próximos,primer,primera,primero,primeros,principalmente,propia,propias,propio,propios,pudo,pueda,puede,pueden,pues,qué,que,quedó,queremos,quién,quien,quienes,quiere,realizó,realizado,realizar,respecto,sí,sólo,se,señaló,sea,sean,según,segunda,segundo,seis,ser,será,serán,sería,si,sido,siempre,siendo,siete,sigue,siguiente,sin,sino,sobre,sola,solamente,solas,solo,solos,son,su,sus,tal,también,tampoco,tan,tanto,tenía,tendrá,tendrán,tenemos,tener,tenga,tengo,tenido,tercera,tiene,tienen,toda,todas,todavía,todo,todos,total,tras,trata,través,tres,tuvo,un,una,unas,uno,unos,usted,va,vamos,van,varias,varios,veces,ver,vez,y,ya,yo'stop_words = stop_words.split(','); clean_text = this.text.replace(/[\.,-\/#!$%\^&\*;:{}=\-_`~()]/g,''); tokens = clean_text.split(' ');for(var i = 0 ; i< tokens.length ; i++){if(stop_words.indexOf(tokens[i])==-1){emit( tokens[i],1 );}}}"      
        location = request.POST["new_location"]
        mapfunc = """
            function() {
                stop_words = ["que","no","El","La","del","de","a","","y","o","se","RT","alguna","algunas","alguno","algunos","ambos","ampleamos","ante","antes","aquel","aquellas","aquellos","aqui","arriba","atras","bajo","bastante","bien","cada","cierta","ciertas","cierto","ciertos","como","con","conseguimos","conseguir","consigo","consigue","consiguen","consigues","cual","cuando","dentro","desde","donde","dos","el","ellas","ellos","empleais","emplean","emplear","empleas","empleo","en","encima","entonces","entre","era","eramos","eran","eras","eres","es","esta","estaba","estado","estais","estamos","estan","estoy","fin","fue","fueron","fui","fuimos","gueno","ha","hace","haceis","hacemos","hacen","hacer","haces","hago","incluso","intenta","intentais","intentamos","intentan","intentar","intentas","intento","ir","la","largo","las","lo","los","mientras","mio","modo","muchos","muy","nos","nosotros","otro","para","pero","podeis","podemos","poder","podria","podriais","podriamos","podrian","podrias","por","porque","primero","puede","pueden","puedo","quien","sabe","sabeis","sabemos","saben","saber","sabes","ser","si","siendo","sin","sobre","sois","solamente","solo","somos","soy","su","sus","teneis","tenemos","tener","tengo","tiempo","tiene","tienen","todo","trabaja","trabajais","trabajamos","trabajan","trabajar","trabajas","trabajo","tras","tuyo","ultimo","un","una","unas","uno","unos","usa","usais","usamos","usan","usar","usas","uso","va","vais","valor","vamos","van","vaya","verdad","verdadera","verdadero","vosotras","vosotros","voy","yo"]
                clean_text = this.text.replace(/[\.',-\/#!$%\^&\*;:{}=\-_`~()]/g,'');
                tokens = clean_text.split(' ');
                for(var i = 0 ; i< tokens.length ; i++)
                {
                    if(stop_words.indexOf(tokens[i])==-1)
                    {
                        emit( tokens[i],1 );
                    }
                }
            }
        """
        mapfunc = mapfunc.decode("utf8")

        reducefunc = """
            function reduce(key, values) {
              return values.length;
            }
        """
        print "LLEGO"
        name = request.user.username

        try:
            Tweet.objects.filter(zone=location).map_reduce(mapfunc, reducefunc, 'twitter_mapreducetrendings')
            result = MapReduceTrendings.objects.all().order_by('-value')[:50]
            trendings = []
            for trending in result:
                word = trending.id.replace(u"‘","").replace(u"“","").replace(u"…","")
                t = {"word":word, "cont":trending.value}
                trendings.append(t)
            response_dict = {"trendings":trendings}
            
        except Exception as e:
            print e
            response_dict = {} 
        return HttpResponse(json.dumps(response_dict), content_type='application/javascript')
        # return render_to_response('twitter/home.html', {'name':name,'tweets':tweets},context_instance=RequestContext(request))
    else:
        return render_to_response('twitter/home.html', context_instance=RequestContext(request))