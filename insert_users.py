from pymongo import MongoClient
import json
import time
from datetime import datetime
import random

PATH = '/home/miguel/Dropbox/cloud_data/comments2'
data_raw = open(PATH,'r').read()
data_json = json.loads(data_raw)

users = []
usernames = []

tweets = []

zones=[
		{"loc": {"latitude":-16.394515, "longtitude":-71.533423}, "name":"Arequipa"},
		{"loc": {"latitude":-12.05678, "longtitude":-77.044888}, "name":"Lima"},
		{"loc": {"latitude":-13.516878, "longtitude":-71.978559}, "name":"Cusco"},
		{"loc": {"latitude":-18.014161, "longtitude":-70.2495}, "name":"Tacna"},
		{"loc": {"latitude":-15.836765, "longtitude":-70.029774}, "name":"Puno"}
	]

contador= 1
for elem in data_json:
	print (contador)
	contador+=1

	#USUARIOS
	if elem["user_username"] not in usernames:
		first_name = elem["user_username"]
		last_name = elem["user_username"]
		names = elem["user_name"].split()
		try:
			first_name = names[0]
		except:
			pass
		try:
			last_name = names[1]
		except:
			pass
		user = {
			"username" : elem["user_username"],
			"first_name" : first_name,
			"last_name" : last_name,
			"account_type" : "public",
			"is_active" : True,
			"location" : "aqp",
			"is_admin" : False,
			"password" : "pbkdf2_sha256$10000$5rE2FWfbzDnq$PmlA8xa3UHD+TgjmgkWsl6om6gIPOqFSu20xfkslYjA=",
			"email" : elem["user_username"]+"@gmail.com"
		}

		users.append(user)
		usernames.append(elem["user_username"])

	mentions = []
	for mention_user in elem["user_mentions"]:
		mentions.append(mention_user["screen_name"])
		if mention_user["screen_name"] not in usernames:
			first_name = mention_user["screen_name"]
			last_name = mention_user["screen_name"]
			names = mention_user["name"].split()
			try:
				first_name = names[0]
			except:
				pass
			try:
				last_name = names[1]
			except:
				pass
			user_mention = {
				"username" : mention_user["screen_name"],
				"first_name" : first_name,
				"last_name" : last_name,
				"account_type" : "public",
				"is_active" : True,
				"location" : "aqp",
				"is_admin" : False,
				"password" : "pbkdf2_sha256$10000$5rE2FWfbzDnq$PmlA8xa3UHD+TgjmgkWsl6om6gIPOqFSu20xfkslYjA=",
				"email" : mention_user["screen_name"]+"@gmail.com"
			}
			users.append(user_mention)
			usernames.append(mention_user["screen_name"])
	hashtags = []
	for hashtag in elem["hashtags"]:
		hashtags.append( hashtag["text"])

	#TWEETS
	ts = datetime.strptime(elem["date"],'%a %b %d %H:%M:%S +0000 %Y')
	# print t
	temp_zone = random.choice(zones)
	tweet = {
	
		"created_on" : ts,
		"text": elem["text"],
		"user_owner": elem["user_username"],
		"user_mentions": mentions,
		"hashtags": hashtags,
		"location": temp_zone["loc"],
		"zone": temp_zone["name"]
		
	}

	tweets.append(tweet)
# print (users)
# print("\n\n\n\n\n\n")
# print(tweets)


client = MongoClient()

db = client.twitter
collection_tweet = db.twitter_tweet
collection_user = db.twitter_twitteruser
collection_zone = db.twitter_zone
# user = {
# 	"username" : "gg",
# 	"first_name" : "miguel",
# 	"last_name" : "bustamante",
# 	"account_type" : "public",
# 	"is_active" : True,
# 	"location" : "aqp",
# 	"is_admin" : False,
# 	"password" : "pbkdf2_sha256$10000$5rE2FWfbzDnq$PmlA8xa3UHD+TgjmgkWsl6om6gIPOqFSu20xfkslYjA=",
# 	"email" : "gg@gmail.com"
# }

collection_tweet.insert(tweets)
collection_user.insert(users)
collection_zone.insert(zones)


# print(user_id)